package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;
import ru.tsc.kitaev.tm.service.dto.UserDTOService;
import ru.tsc.kitaev.tm.util.HashUtil;

public class UserServiceTest {

    @NotNull
    private final IUserDTOService userService;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @Nullable
    private final String userHashPassword;

    @NotNull
    private final String secret;

    @NotNull
    private final Integer iteration;

    public UserServiceTest() {
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull final ConnectionService connectionService = new ConnectionService(propertyService);
        @NotNull final LogService logService = new LogService();
        secret = propertyService.getPasswordSecret();
        iteration = propertyService.getPasswordIteration();
        userService = new UserDTOService(connectionService, logService, propertyService);
        @NotNull final String userPassword = "userTest";
        @NotNull String userEmail = "test@mail.com";
        user = userService.create(userLogin, userPassword, userEmail);
        userId = user.getId();
        userHashPassword = HashUtil.salt(secret, iteration, userPassword);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserTest() {
        @NotNull final UserDTO newUser = userService.findByLogin(userLogin);
        Assert.assertEquals(userLogin, newUser.getLogin());
        Assert.assertEquals(userHashPassword, newUser.getPasswordHash());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLoginTest() {
        userService.removeByLogin(userLogin);
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserTest";
        @NotNull final String newUserEmail = "newTest@mail.com";
        @NotNull final String newUserPassword = "newPassword";
        @NotNull final UserDTO newUser = userService.create(newUserLogin, newUserPassword, newUserEmail);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        Assert.assertEquals(newUserEmail, newUser.getEmail());
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    @Category(UnitCategory.class)
    public void setPasswordTest() {
        Assert.assertEquals(userService.findById(userId).getPasswordHash(), userHashPassword);
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        @NotNull final UserDTO user = userService.findById(userId);
        Assert.assertNotEquals(userHashPassword, user.getPasswordHash());
        Assert.assertEquals(HashUtil.salt(secret, iteration, newPassword), user.getPasswordHash());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateUserTest() {
        @NotNull final String newFirstName = "newFirstNameTest";
        @NotNull final String newLastName = "newLastNameTest";
        @NotNull final String newMiddleName = "newMiddleNameTest";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        userService.updateUser(userId, newFirstName, newLastName, newMiddleName);
        @NotNull final UserDTO updUser = userService.findById(userId);
        Assert.assertEquals(newFirstName, updUser.getFirstName());
        Assert.assertEquals(newLastName, updUser.getLastName());
        Assert.assertEquals(newMiddleName, updUser.getMiddleName());
    }

    @Test
    @Category(UnitCategory.class)
    public void lockUnlockByLoginTest() {
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(userService.findByLogin(userLogin).getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(userService.findByLogin(userLogin).getLocked());
    }

    @After
    public void after() {
        if (userService.isLoginExists(userLogin)) userService.removeById(userId);
    }

}
