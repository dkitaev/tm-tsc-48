package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.Domain;

import java.sql.SQLException;

public interface IDomainService {

    @NotNull
    public Domain getDomain();

    public void setDomain(@Nullable Domain domain) throws SQLException;

    public void dataBackupLoad();

    public void dataBackupSave();

    public void dataBase64Load();

    public void dataBase64Save();

    public void dataBinLoad();

    public void dataBinSave();

    public void dataJsonLoadFasterXML();

    public void dataJsonSaveFasterXML();

    public void dataJsonLoadJaxB();

    public void dataJsonSaveJaxB();

    public void dataXmlLoadFasterXML();

    public void dataXmlSaveFasterXML();

    public void dataXmlLoadJaxB();

    public void dtaXmlSaveJaxB();

    public void dataYamlLoadFasterXML();

    public void dataYamlSaveFasterXML();

}
