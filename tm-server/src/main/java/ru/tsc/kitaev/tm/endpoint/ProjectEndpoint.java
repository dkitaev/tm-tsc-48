package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().clear(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().findAll(session.getUserId());
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAllSorted(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().findAll(session.getUserId(), sort);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().findById(session.getUserId(), id);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectDTO findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().findByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().create(session.getUserId(), name, description);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().startByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().finishById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().finishByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().finishByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().changeStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectDTOService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().existsById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public boolean existsProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") final int index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectDTOService().existsByIndex(session.getUserId(), index);
    }

}
