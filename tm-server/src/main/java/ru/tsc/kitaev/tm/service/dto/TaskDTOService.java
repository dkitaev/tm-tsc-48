package ru.tsc.kitaev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskDTOService implements ITaskDTOService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public TaskDTOService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findAllByUserId(userId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final String sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<TaskDTO> comparator = sortType.getComparator();
            return taskRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findById(userId, id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeByIndex(userId, index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final TaskDTO task = taskRepository.findById(userId, id);
            return task != null;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, final int index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            taskRepository.findByIndex(userId, index);
            return true;
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.getSize(userId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public void addAll(@NotNull final List<TaskDTO> tasks) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            for (TaskDTO task : tasks) {
                taskRepository.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            return taskRepository.findByName(userId, name);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final TaskDTO task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByIndex(userId, index);
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final TaskDTO task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByIndex(userId, index);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByName(userId, name);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final TaskDTO task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByIndex(userId, index);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByName(userId, name);
            task.setStatus(Status.COMPLETED);
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @Nullable final TaskDTO task = findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(status);
            task.setStartDate(new Date());
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByIndex(userId, index);
            task.setStatus(status);
            task.setStartDate(new Date());
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final TaskDTO task = findByName(userId, name);
            task.setStatus(status);
            task.setStartDate(new Date());
            if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
