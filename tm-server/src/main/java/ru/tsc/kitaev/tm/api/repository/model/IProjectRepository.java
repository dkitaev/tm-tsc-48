package ru.tsc.kitaev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void add(@NotNull final Project project);

    void update(@NotNull final Project project);

    void clear();

    void clearByUserId(@NotNull final String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUserId(@NotNull final String userId);

    @NotNull
    Project findById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Project findByIndex(@NotNull final String userId, @NotNull final Integer index);

    void removeById(@NotNull final String userId, @NotNull final String id);

    void removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    @NotNull
    Integer getSize(@NotNull final String userId);

    @NotNull
    Project findByName(@NotNull final String userId, @NotNull final String name);

    void removeByName(@NotNull final String userId, @NotNull final String name);

}
