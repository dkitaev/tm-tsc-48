package ru.tsc.kitaev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDTORepository implements IUserDTORepository {

    protected EntityManager entityManager;

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull UserDTO user) {
        entityManager.persist(user);
    }

    @Override
    public void update(@NotNull UserDTO user) {
        entityManager.merge(user);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDTO")
                .executeUpdate();
    }

    @Override
    public void remove(@NotNull UserDTO user) {
        entityManager.remove(user);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull String id) {
        return entityManager
                .createQuery("FROM UserDTO u WHERE u.id = :id", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public UserDTO findByIndex(@NotNull Integer index) {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("FROM UserDTO u WHERE u.login = :login", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public UserDTO findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("FROM UserDTO u WHERE u.email = :email", UserDTO.class)
                .setParameter("email", email)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

}
