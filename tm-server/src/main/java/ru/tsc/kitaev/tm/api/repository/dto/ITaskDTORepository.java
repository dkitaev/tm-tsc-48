package ru.tsc.kitaev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskDTORepository {

    void add(@NotNull final TaskDTO task);

    void update(@NotNull final TaskDTO task);

    void clear();

    void clearByUserId(@NotNull final String userId);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    TaskDTO findById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    TaskDTO findByIndex(@NotNull final String userId, @NotNull final Integer index);

    void removeById(@NotNull final String userId, @NotNull final String id);

    void removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    @NotNull
    Integer getSize(@NotNull final String userId);

    @NotNull
    TaskDTO findByName(@NotNull final String userId, @NotNull final String name);

    void removeByName(@NotNull final String userId, @NotNull final String name);

    @NotNull
    List<TaskDTO> findAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

}
