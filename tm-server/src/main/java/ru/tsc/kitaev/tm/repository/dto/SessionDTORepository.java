package ru.tsc.kitaev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.SessionDTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.List;

public class SessionDTORepository implements ISessionDTORepository {

    protected EntityManager entityManager;

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO")
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll() {
        return entityManager
                .createQuery("FROM SessionDTO", SessionDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    public @Nullable SessionDTO findById(@NotNull String id) {
        return entityManager
                .createQuery("FROM SessionDTO s WHERE s.id = :id", SessionDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(s) FROM ProjectDTO s", Long.class)
                .getSingleResult()
                .intValue();
    }

}
