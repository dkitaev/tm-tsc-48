package ru.tsc.kitaev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.ProjectDTO;

import java.util.List;

public interface IProjectDTORepository {

    void add(@NotNull final ProjectDTO project);

    void update(@NotNull final ProjectDTO project);

    void clear();

    void clearByUserId(@NotNull final String userId);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAllByUserId(@NotNull final String userId);

    @Nullable
    ProjectDTO findById(@NotNull final String userId, @NotNull final String id);

    @NotNull
    ProjectDTO findByIndex(@NotNull final String userId, @NotNull final Integer index);

    void removeById(@NotNull final String userId, @NotNull final String id);

    void removeByIndex(@NotNull final String userId, @NotNull final Integer index);

    @NotNull
    Integer getSize(@NotNull final String userId);

    @NotNull
    ProjectDTO findByName(@NotNull final String userId, @NotNull final String name);

    void removeByName(@NotNull final String userId, @NotNull final String name);

}
