package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public List<TaskDTO> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        return serviceLocator.getProjectTaskDTOService().findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public void removeById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().removeById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getProjectTaskDTOService().removeByName(session.getUserId(), name);
    }

}
