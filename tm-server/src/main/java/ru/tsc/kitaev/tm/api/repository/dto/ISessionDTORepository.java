package ru.tsc.kitaev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.SessionDTO;

import java.util.List;

public interface ISessionDTORepository {

    void add(@NotNull final SessionDTO session);

    void clear();

    @NotNull
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findById(@NotNull final String id);

    void removeById(@NotNull final String id);

    int getSize();

}
