package ru.tsc.kitaev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.UserDTO;

import java.util.List;

public interface IUserDTOService {

    void clear();

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@NotNull final String id);

    @NotNull
    UserDTO findByIndex(@NotNull final Integer index);

    void removeById(@NotNull final String id);

    boolean existsById(@NotNull final String id);

    boolean existsByIndex(final int index);

    void addAll(@NotNull List<UserDTO> users);

    @NotNull
    UserDTO findByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    @NotNull
    UserDTO findByEmail(@Nullable String email);

    boolean isEmailExists(@Nullable String email);

    void removeByLogin(@Nullable String login);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void setPassword(@Nullable String userId, @Nullable String password);

    void setPassword(@NotNull UserDTO user, @NotNull String password);

    void updateUser(@Nullable String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
