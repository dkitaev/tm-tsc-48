package ru.tsc.kitaev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.dto.TaskDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;

import java.util.List;

public interface IUserDTORepository {

    void add(@NotNull final UserDTO user);

    void update(@NotNull final UserDTO user);

    void clear();

    void remove(@NotNull final UserDTO user);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@NotNull final String id);

    @NotNull
    UserDTO findByIndex(@NotNull final Integer index);

    @NotNull
    UserDTO findByLogin(@NotNull final String login);

    @NotNull
    UserDTO findByEmail(@NotNull final String email);

}
