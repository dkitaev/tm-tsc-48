package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;

public interface ISessionRepository {

    @Nullable
    SessionDTO getSession();

    void setSession(@Nullable final SessionDTO session);

}
