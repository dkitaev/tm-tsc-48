package ru.tsc.kitaev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "update-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update user profile...";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().updateUser(session, firstName, lastName, middleName);
        System.out.println("[OK]");
    }

}
