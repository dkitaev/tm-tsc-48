package ru.tsc.kitaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractProjectCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter id");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().removeProjectById(session, id);
        serviceLocator.getProjectTaskEndpoint().removeById(session, id);
        System.out.println("[OK]");
    }

}
