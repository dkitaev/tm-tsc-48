package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;

public class SessionRepository implements ISessionRepository {

    @Nullable
    private SessionDTO session;

    @Override
    @Nullable
    public SessionDTO getSession() {
        return session;
    }

    @Override
    public void setSession(@Nullable final SessionDTO session) {
        this.session = session;
    }

}
