package ru.tsc.kitaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.command.AbstractTaskCommand;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;
import ru.tsc.kitaev.tm.util.TerminalUtil;

public final class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter Name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[REMOVE PROJECT BY NAME]");
        serviceLocator.getTaskEndpoint().removeTaskByName(session, name);
        System.out.println("[OK]");
    }

}
